'use strict';

var fApp = angular.module('fApp', ['ngGrid', 'ngQuickDate', 'ui.utils', 'ui.bootstrap', 'ui.router', 'ui.select2']);

// bootstrap (pre instance)
fApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider
        .otherwise('/topics');

    $httpProvider.defaults.headers.put = {
        'Content-Type': 'application/json'
    };
    $httpProvider.defaults.headers.post = {
        'Content-Type': 'application/json'
    };

    $stateProvider.state('register', {
        url: '/register',
        templateUrl: 'resources/components/security/register.html',
        controller: 'register'
    });

    //////////////////////////Topic////////////////////////////////
    $stateProvider.state('topics', {
        url: '/topics',
        templateUrl: 'resources/components/topic/topics.html',
        controller: 'topics'
    });
    $stateProvider.state('createTopic', {
        url: '/createTopic',
        templateUrl: 'resources/components/topic/createTopic.html',
        controller: 'createTopic'
    });
    $stateProvider.state('updateTopic', {
        url: '/updateTopic/{topicId}',
        templateUrl: 'resources/components/topic/updateTopic.html',
        controller: 'updateTopic'
    });
    $stateProvider.state('deleteTopic', {
        url: '/deleteTopic/{topicId}',
        templateUrl: 'resources/components/topic/deleteTopic.html',
        controller: 'deleteTopic'
    });

    //////////////////////////Message////////////////////////////////
    $stateProvider.state('messages', {
        url: '/messages/{topicId}',
        templateUrl: 'resources/components/message/messages.html',
        controller: 'messages'
    });
    $stateProvider.state('createMessage', {
        url: '/createMessage/{topicId}',
        templateUrl: 'resources/components/message/createMessage.html',
        controller: 'createMessage'
    });
    $stateProvider.state('updateMessage', {
        url: '/updateMessage/{messageId}/{topicId}',
        templateUrl: 'resources/components/message/updateMessage.html',
        controller: 'updateMessage'
    });
    $stateProvider.state('deleteMessage', {
        url: '/deleteMessage/{messageId}/{topicId}',
        templateUrl: 'resources/components/message/deleteMessage.html',
        controller: 'deleteMessage'
    });



}]);


// once app is instantiated
fApp.run(['constants', '$http', function (constants) {

    // set window title
    document.title = constants.appTitle;
}]);