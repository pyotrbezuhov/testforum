'use strict';
fApp.controller('topics', ['$scope', '$filter', '$state', '$log', '$http', '$location', '$rootScope', function ($scope, $filter, $state, $log, $http, $location, $rootScope) {
    $scope.loading = false;
    $scope.topics = [];
    $scope.header = 'Topics';

    $log.log("topicsController %o", $scope);


    $scope.createTopic = function () {
        $log.log("createTopic");
        $location.path('/createTopic');
    };
    $scope.updateTopic = function (topic) {
        $log.log("updateTopic %o", topic);
        $state.transitionTo('updateTopic', {topicId: topic.id});
    };
    $scope.deleteTopic = function (topic) {
        $log.log("deleteTopic %o", topic);
        $state.transitionTo('deleteTopic', {topicId: topic.id});
    };

    $scope.showTopicMessages = function (id) {
        $log.log("onselectRow %o", id);
        $state.transitionTo('messages', {topicId: id});
    };

    function updateFormatter(cellvalue, options, rowObject) {
        $log.log("updateFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);

        var result = '';
        if (rowObject.user.username == $rootScope.username)
            result = '<button class="btn btn-small btn-primary" style="width: 100%;">Edit</button>';
        return result;
    }

    function deleteFormatter(cellvalue, options, rowObject) {
        $log.log("deleteFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);

        var result = '';
        if (rowObject.user.username == $rootScope.username)
            result = '<button class="btn btn-small btn-danger" style="width: 100%;">Delete</button>';
        return result;
    }

    function dateFormatter(cellvalue, options, rowObject) {
        return cellvalue ? new Date(cellvalue).toLocaleString() : '';
    }

    var options = {
        url: '/topics/listGrid',
        datatype: "json",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        colNames: ['Content', 'Create Date', 'Update Date', '', ''],
        colModel: [
            {name: 'content', index: 'content', width: 200, sortable: false},
            {name: 'createDate', index: 'createDate', sortable: true, formatter: dateFormatter, defaultValue: null, width: 160},
            {name: 'updateDate', index: 'updateDate', sortable: true, formatter: dateFormatter, defaultValue: null, width: 160},
            {name: 'id', index: 'id', align: "center", width: 80, formatter: updateFormatter, sortable: false, search: false},
            {name: 'id', index: 'id', align: "center", width: 80, formatter: deleteFormatter, sortable: false, search: false}
        ],
        jsonReader: {
            root: "topicData",
            page: "currentPage",
            total: "totalPages",
            records: "totalRecords",
            repeatitems: false,
            id: "id"
        },
        rowNum: 3,
        //rowTotal: 50,
        rowList: [3, 20, 30],
        altRows: true,
        pager: '#topics-pager',
        sortname: 'id',
        loadonce: false,
        viewrecords: true,
        sortorder: "desc",
        height: 500,
        width: 1200,
        caption: 'Topics',
        onCellSelect: function (rowid, iCol, cellcontent, e) {
            $log.log("onCellSelect %o %o", iCol, cellcontent);

            if (iCol == 3 && rowObject.user.username == $rootScope.username)
                $state.transitionTo('updateTopic', {topicId: rowid});
            else if (iCol == 4 && rowObject.user.username == $rootScope.username)
                $state.transitionTo('deleteTopic', {topicId: rowid});

        },
        ondblClickRow: function (id) {
            $scope.showTopicMessages(id);
        }
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        $scope.loading = true;

        $http.get('username').then(function (result, status, headers, config) {
            $scope.username = result.data;
            $rootScope.username = result.data;
            $("#topics-list").jqGrid(options).navGrid('#topics-pager', {edit: false, add: false, del: false, search: false}); // options

            $scope.loading = false;
        });
    });
}]);

fApp.controller('createTopic', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.topic = {};
    $scope.header = 'Create Topic';

    $log.log("creatTopicController %o", $scope);

    $scope.createTopic = function () {

        $http({method: 'POST', url: 'topics', data: $scope.topic, headers: { 'Content-Type': 'application/json; charset=utf-8'} })
            .success(function (result, status, headers, config) {
                $location.path('/topics');
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    };

    $scope.cancel = function () {
        $location.path('/topics');
    };

}]);

fApp.controller('updateTopic', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.topic = {};
    $scope.header = 'Update Topic';

    $log.log("updateTopicController %o", $scope);

    $scope.updateTopic = function () {

        $http({method: 'PUT', url: 'topics/' + $scope.topic.id, data: $scope.topic, headers: { 'Content-Type': 'application/json; charset=utf-8'}})
            .success(function (result, status, headers, config) {
                $location.path('/topics');
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    }

    $scope.cancel = function () {
        $location.path('/topics');
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        $scope.loading = true;

        if (newvalue && newvalue.topicId) {
            $http.get('topics/' + newvalue.topicId).then(function (result, status, headers, config) {
                $log.log(result);
                $scope.topic = result.data;
                $scope.loading = false;
            });
        } else {
            $location.path('/topics');
        }
    });
}]);

fApp.controller('deleteTopic', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.topicId = null;
    $scope.header = 'Delete Topic';

    $log.log("deleteTopicController %o", $scope);

    $scope.deleteTopic = function () {
        $log.log('delete topic');
        $http({method: 'DELETE', url: 'topics/' + $scope.topicId, data: null, headers: { 'Content-Type': 'application/json; charset=utf-8'}})
            .success(function (result, status, headers, config) {
                $location.path('/topics');
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    }

    $scope.cancel = function () {
        $location.path('/topics');
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        if (newvalue && newvalue.topicId) {
            $scope.topicId = newvalue.topicId;
        } else {
            $location.path('/topics');
        }
    });
}]);