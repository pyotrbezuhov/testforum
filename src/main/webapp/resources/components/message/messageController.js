'use strict';

fApp.controller('messages', ['$scope', '$filter', '$state', '$log', '$http', '$location', '$rootScope', function ($scope, $filter, $state, $log, $http, $location, $rootScope) {
    $scope.loading = false;
    $scope.topic = null;
    $scope.header = 'Messages';
    $scope.username = $rootScope.username;

    $log.log("messageController %o", $scope);

    $scope.createMessage = function () {
        $log.log("createMessage");
        $state.transitionTo('createMessage', {topicId: $scope.topic.id});
    };
    $scope.updateMessage = function (message) {
        $log.log("updateMessage %o", message);
        $state.transitionTo('updateMessage', {topicId: $scope.topic.id, messageId: message.id});
    };
    $scope.deleteMessage = function (message) {
        $log.log("deleteMessage %o", message);
        $state.transitionTo('deleteMessage', {topicId: $scope.topic.id, messageId: message.id});
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        $scope.loading = true;
        $log.log("messages newvalue %o", newvalue);
        if (newvalue.topicId) {
            $scope.header = 'Loading';
            $scope.url = 'topics/' + newvalue.topicId + '/withMessages';
            $http.get($scope.url).then(function (result) {
                $log.log(result);
                $scope.loading = false;
                $scope.header = result.content;
                $scope.topic = result.data;

                if (!$scope.topic) $location.path('/topics');
            });
        } else {
            $location.path('/topics');
        }
    });
}]);

fApp.controller('createMessage', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.message = { topic: {}};
    $scope.header = 'Create Message';

    $log.log("createmessageController %o", $scope);

    $scope.createMessage = function () {
        $scope.loading = true;

        $http({method: 'POST', url: 'messages', data: $scope.message, headers: { 'Content-Type': 'application/json; charset=utf-8'} })
            .success(function (result, status, headers, config) {
                $state.transitionTo('messages', {topicId: $scope.message.topic.id});
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    };

    $scope.cancel = function () {
        $state.transitionTo('messages', {topicId: $scope.message.topic.id});
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        if (newvalue && newvalue.topicId) {
            $scope.message.topic.id = newvalue.topicId;
        } else {
            $state.transitionTo('messages', {topicId: $scope.message.topic.id});
        }
    });
}]);

fApp.controller('updateMessage', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.topicId = null;
    $scope.message = {};
    $scope.header = 'Update Message';

    $log.log("updatemessageController %o", $scope);

    $scope.updateMessage = function () {
        $scope.loading = true;

        $http({method: 'PUT', url: 'messages/' + $scope.message.id, data: $scope.message, headers: { 'Content-Type': 'application/json; charset=utf-8'}})
            .success(function (result, status, headers, config) {
                $scope.message = result.data;
                $state.transitionTo('messages', {topicId: $scope.topicId});
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    }

    $scope.cancel = function () {
        $state.transitionTo('messages', {topicId: $scope.message.topic.id});
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        $scope.loading = true;

        if (newvalue && newvalue.messageId && newvalue.topicId) {
            $scope.topicId = newvalue.topicId;
            $http.get('messages/' + newvalue.messageId).then(function (result, status, headers, config) {
                $log.log(result);
                $scope.message = result.data;
                $scope.loading = false;
            });
        } else {
            $state.transitionTo('messages', {topicId: $scope.topicId});
        }
    });
}]);

fApp.controller('deleteMessage', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;

    $scope.messageId = null;
    $scope.topicId = null;
    $scope.header = 'Delete Message';

    $log.log("deleteController %o", $scope);

    $scope.deleteMessage = function () {
        $log.log('delete message');
        $http({method: 'DELETE', url: 'messages/' + $scope.messageId, data: null, headers: { 'Content-Type': 'application/json; charset=utf-8'}})
            .success(function (result, status, headers, config) {
                $state.transitionTo('messages', {topicId: $scope.topicId});
            }).error(function (result, status, headers, config) {
                document.location.href = "login";
            });
    }

    $scope.cancel = function () {
        $state.transitionTo('messages', {topicId: $scope.topicId});
    };

    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        if (newvalue && newvalue.messageId && newvalue.topicId) {
            $scope.messageId = newvalue.messageId;
            $scope.topicId = newvalue.topicId;
        } else {
            $state.transitionTo('messages', {topicId: $scope.messageId});
        }
    });
}]);


//function updateFormatter(cellvalue, options, rowObject) {
//    $log.log("updateFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);
//    var result = '<button ng-click="updateMessage(' + cellvalue + ',' + rowObject + ')" class="fm-button" style="width: 100%; display: block;">Update</button>';
//    return result;
//}
//function deleteFormatter(cellvalue, options, rowObject) {
//    $log.log("deleteFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);
//    var result = '<button ng-click="deleteMessage(' + cellvalue + ')" class="fm-button" style="width: 100%; display: block;">Delete</button>';
//    return result;
//}