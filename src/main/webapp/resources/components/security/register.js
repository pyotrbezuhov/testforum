fApp.controller('register', ['$scope', '$filter', '$state', '$log', '$http', '$location', function ($scope, $filter, $state, $log, $http, $location) {
    $scope.loading = false;
    $scope.user = {};
    $scope.header = 'Registration';

    $scope.register = function () {
        $scope.loading = true;

        $log.log($scope.user);
        $http({method: 'POST', url: 'users', data: $scope.user, headers: { 'Content-Type': 'application/json; charset=utf-8'} })
            .success(function (result, status, headers, config) {
                document.location.href = "login";
            }).error(function (result, status, headers, config) {
                //$location.path('/topics');
                $scope.loading = false;
            });
    };

    $scope.cancel = function () {
        $location.path('/topics');
    };

}]);