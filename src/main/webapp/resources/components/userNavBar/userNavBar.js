'use strict';

fApp.directive('userNavBar', ['$filter', '$rootScope', '$log', '$http', '$state', function ($filter, $rootScope, $log, $http, $state) {
    return {
        restrict: 'A',
        replace: true,
        terminal: true,
        templateUrl: 'resources/components/userNavBar/userNavBar.html',
        scope: {
        },
        link: function (scope, element, attrs) {
            $log.log($rootScope);
            scope.$rootScope = $rootScope;
        }
    };
}]);