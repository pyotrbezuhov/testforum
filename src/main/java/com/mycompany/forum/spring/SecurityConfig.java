package com.mycompany.forum.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.accept.ContentNegotiationStrategy;

import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username,password,enabled from forum.users where username = ?")
                .authoritiesByUsernameQuery("select username,authority from forum.authorities where username = ?");
    }

    @Override
    public void setContentNegotationStrategy(final ContentNegotiationStrategy contentNegotiationStrategy)
    {
        ContentNegotiationManagerFactoryBean managerFactoryBean = new ContentNegotiationManagerFactoryBean();
        managerFactoryBean.setFavorPathExtension(false);
        Properties properties = new Properties();
        properties.put("json", MediaType.APPLICATION_JSON_VALUE);
        properties.put("xml", MediaType.APPLICATION_XML_VALUE);
        managerFactoryBean.setMediaTypes(properties);
        managerFactoryBean.setDefaultContentType(MediaType.APPLICATION_JSON);

        super.setContentNegotationStrategy(managerFactoryBean.getObject());
    }

    @Override
    public void configure(WebSecurity web)
    {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable().authorizeRequests()
            .antMatchers(HttpMethod.POST, "/users").permitAll()
            .antMatchers(HttpMethod.POST, "/**").authenticated()
            .antMatchers(HttpMethod.PUT, "/**").authenticated()
            .antMatchers(HttpMethod.DELETE, "/**").authenticated();
        http//.csrf().disable()
                .formLogin().and()
                .httpBasic();
    }

//    @Bean
//    public RoleHierarchy roleHierarchy()
//    {
//        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
//        roleHierarchy.setHierarchy("ADMIN > STAFF > USER");
//        return roleHierarchy;
//    }
//
//    @Bean
//    public RoleVoter roleVoter()
//    {
//        return new RoleHierarchyVoter(roleHierarchy());
//    }
}
