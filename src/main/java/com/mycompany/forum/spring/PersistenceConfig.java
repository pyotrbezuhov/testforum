package com.mycompany.forum.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.mycompany.forum.persistence"})
//@PropertySource({"file:///${catalina.home}/conf/Forum/persistence.properties"})
@PropertySource({"classpath:/persistence.properties"})
@EnableJpaRepositories("com.mycompany.forum.persistence.repositories")
@EnableSpringDataWebSupport
public class PersistenceConfig
{
    static
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    private Environment env;

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator()
    {
        return new HibernateExceptionTranslator();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException
    {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();

        em.setDataSource(dataSource());
        em.setPackagesToScan("com.mycompany.forum.persistence.entities");

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws PropertyVetoException
    {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public DataSource dataSource() throws PropertyVetoException
    {
//        final ComboPooledDataSource dataSource = new ComboPooledDataSource();
//
//        dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
//        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
//        dataSource.setUser(env.getProperty("jdbc.username"));
//        dataSource.setPassword(env.getProperty("jdbc.password"));
//
//        dataSource.setAcquireIncrement(Integer.parseInt(env.getProperty("c3p0.AcquireIncrement")));//Default: 3 Determines how many connections at a time c3p0 will try to acquire when the pool is exhausted.
//        dataSource.setAcquireRetryAttempts(Integer.parseInt(env.getProperty("c3p0.AcquireRetryAttempts")));// Default: 30 Defines how many times c3p0 will try to acquire a new Connection from the database before giving up. If this value is less than or equal to zero, c3p0 will keep trying to fetch a Connection indefinitely.
//        dataSource.setAcquireRetryDelay(Integer.parseInt(env.getProperty("c3p0.AcquireRetryDelay"))); //Default: 1000  Milliseconds, time c3p0 will wait between acquire attempts.
//        dataSource.setMaxPoolSize(Integer.parseInt(env.getProperty("c3p0.MaxPoolSize"))); //
//        dataSource.setInitialPoolSize(Integer.parseInt(env.getProperty("c3p0.InitialPoolSize")));
//        dataSource.setMaxIdleTime(Integer.parseInt(env.getProperty("c3p0.MaxIdleTime")));
//        dataSource.setMaxStatements(Integer.parseInt(env.getProperty("c3p0.MaxStatements")));
//        dataSource.setMinPoolSize(Integer.parseInt(env.getProperty("c3p0.MinPoolSize")));

//        return dataSource;

        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("classpath:forum.sql")
                .build();
    }

    final Properties additionalProperties()
    {
        return new Properties()
        {
            {
//                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
//                setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
                setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
                setProperty("hibernate.connection.characterEncoding", env.getProperty("hibernate.connection.characterEncoding"));
                setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
                setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
                setProperty("hibernate.use_sql_comments", env.getProperty("hibernate.use_sql_comments"));
                setProperty("hibernate.max_fetch_depth", env.getProperty("hibernate.max_fetch_depth"));
                setProperty("hibernate.jdbc.batch_size", env.getProperty("hibernate.jdbc.batch_size"));
                setProperty("hibernate.jdbc.fetch_size", env.getProperty("hibernate.jdbc.fetch_size"));
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.autoReconnect", env.getProperty("hibernate.autoReconnect"));
                setProperty("hibernate.auto_close_session", env.getProperty("hibernate.auto_close_session"));
            }
        };
    }
}
