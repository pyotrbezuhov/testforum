package com.mycompany.forum.business;

import com.mycompany.forum.web.util.MyConstraintViolation;

import javax.validation.ConstraintViolation;
import java.util.*;

public class Check
{
    private Check()
    {
    }

    public static <T extends Enum<T>> T enumField(Class<T> enumType, String name, Set<ConstraintViolation<?>> set)
    {
        if (name == null || name.trim().equals(""))
        {
            return T.valueOf(enumType, "NONE");
        }

        try
        {
            return T.valueOf(enumType, name.trim());
        } catch (IllegalArgumentException exc)
        {
            String className = enumType.getName();
            className = className.substring(className.lastIndexOf('.') + 1);
            className = Character.toLowerCase(className.charAt(0)) + className.substring(1);
            set.add(new MyConstraintViolation(className, "Invalid " + className));
            return T.valueOf(enumType, "NONE");
        }
    }

    public static <T extends Enum<T>> T enumField(Class<T> enumType, String name)
    {
        if (name == null || name.trim().equals(""))
        {
            return T.valueOf(enumType, "NONE");
        }

        try
        {
            return T.valueOf(enumType, name.trim());
        } catch (IllegalArgumentException exc)
        {
            return T.valueOf(enumType, "NONE");
        }
    }

    public static Long longField(String numberString)
    {
        try
        {
            return Long.parseLong(numberString);
        } catch (NumberFormatException exc)
        {
            return null;
        }
    }
}
