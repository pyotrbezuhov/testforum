package com.mycompany.forum.persistence.repositories;

import com.mycompany.forum.persistence.entities.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TopicRepository extends JpaRepository<Topic, Long>
{
    @Query(value = "select t from Topic t left join fetch t.messageList where t.id=:id")
    public List<Topic> findTopicWithMessages(@Param("id") Long id);
}
