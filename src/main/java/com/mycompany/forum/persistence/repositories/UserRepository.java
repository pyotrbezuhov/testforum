package com.mycompany.forum.persistence.repositories;

import com.mycompany.forum.persistence.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>
{
    @Query(value = "SELECT u FROM User u WHERE u.username = :username")
    public List<User> findByUserName(@Param("username") String username);

    @Query(value = "SELECT u FROM User u WHERE u.username = :username and u.password=:password")
    public List<User> findByUserNameAndPassword(@Param("username") String username, @Param("password") String password);
}
