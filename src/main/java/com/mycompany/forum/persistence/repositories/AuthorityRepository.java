package com.mycompany.forum.persistence.repositories;

import com.mycompany.forum.persistence.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by VAHAN on 10.04.14.
 */
public interface AuthorityRepository extends JpaRepository<Authority, Long>
{

}