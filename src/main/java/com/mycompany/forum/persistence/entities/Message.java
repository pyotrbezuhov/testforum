package com.mycompany.forum.persistence.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author User
 */
@Entity
@Table(name = "messages", catalog = "", schema = "forum")
@XmlRootElement
@NamedQueries(
        {
                @NamedQuery(name = "Message.findAll", query = "SELECT m FROM Message m"),
                @NamedQuery(name = "Message.findByMessageId", query = "SELECT m FROM Message m WHERE m.id = :id"),
                @NamedQuery(name = "Message.findByContent", query = "SELECT m FROM Message m WHERE m.content = :content"),
                @NamedQuery(name = "Message.findByCreateDate", query = "SELECT m FROM Message m WHERE m.createDate = :createDate"),
                @NamedQuery(name = "Message.findByUpdateDate", query = "SELECT m FROM Message m WHERE m.updateDate = :updateDate")
        })
public class Message implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String content;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_date", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @JsonIgnore
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private User user;

    @JoinColumn(name = "topic_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Topic topic;

    public Message()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(final Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getUpdateDate()
    {
        return updateDate;
    }

    public void setUpdateDate(final Date updateDate)
    {
        this.updateDate = updateDate;
    }

    @XmlTransient
    public User getUser()
    {
        return user;
    }

    public void setUser(final User user)
    {
        this.user = user;
    }

    @XmlTransient
    public Topic getTopic()
    {
        return topic;
    }

    public void setTopic(final Topic topic)
    {
        this.topic = topic;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Message))
        {
            return false;
        }
        Message other = (Message) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.Message[ id=" + id + " ]";
    }

}