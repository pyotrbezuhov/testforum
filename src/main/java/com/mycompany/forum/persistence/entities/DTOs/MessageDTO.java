package com.mycompany.forum.persistence.entities.DTOs;

import com.mycompany.forum.persistence.entities.Message;

import java.io.Serializable;
import java.util.Date;


public class MessageDTO implements Serializable
{
    private Long id;

    private String content;

    private Date createDate;

    private Date updateDate;

    private UserDTO user;

    private TopicDTO topic;

    public MessageDTO()
    {
    }

    public MessageDTO(Message message)
    {
        if (message == null) return;

        this.id = message.getId();
        this.content = message.getContent();
        this.createDate = message.getCreateDate();
        this.updateDate = message.getUpdateDate();
        this.user=new UserDTO(message.getUser());
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(final Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getUpdateDate()
    {
        return updateDate;
    }

    public void setUpdateDate(final Date updateDate)
    {
        this.updateDate = updateDate;
    }

    public UserDTO getUser()
    {
        return user;
    }

    public void setUser(final UserDTO user)
    {
        this.user = user;
    }

    public TopicDTO getTopic()
    {
        return topic;
    }

    public void setTopic(final TopicDTO topic)
    {
        this.topic = topic;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessageDTO))
        {
            return false;
        }
        MessageDTO other = (MessageDTO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.Message[ id=" + id + " ]";
    }

}