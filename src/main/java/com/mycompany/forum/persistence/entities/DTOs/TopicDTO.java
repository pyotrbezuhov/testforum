package com.mycompany.forum.persistence.entities.DTOs;

import com.mycompany.forum.persistence.entities.Topic;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class TopicDTO implements Serializable
{
    private Long             id;
    private String           content;
    private Date             createDate;
    private Date             updateDate;
    private UserDTO          user;
    private List<MessageDTO> messageList;

    public TopicDTO()
    {
    }

    public TopicDTO(Topic topic)
    {
        if (topic == null) return;

        this.id = topic.getId();
        this.content = topic.getContent();
        this.createDate = topic.getCreateDate();
        this.updateDate = topic.getUpdateDate();
        this.setUser(new UserDTO(topic.getUser()));
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(final Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getUpdateDate()
    {
        return updateDate;
    }

    public void setUpdateDate(final Date updateDate)
    {
        this.updateDate = updateDate;
    }

    public UserDTO getUser()
    {
        return user;
    }

    public void setUser(final UserDTO user)
    {
        this.user = user;
    }

    public List<MessageDTO> getMessageList()
    {
        return messageList;
    }

    public void setMessageList(final List<MessageDTO> messageList)
    {
        this.messageList = messageList.size() == 0 ? null : messageList;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TopicDTO))
        {
            return false;
        }
        TopicDTO other = (TopicDTO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.Topic[ id=" + id + " ]";
    }

}
