package com.mycompany.forum.persistence.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author User
 */
@Entity
@Table(name = "authorities", catalog = "", schema = "forum", uniqueConstraints =
        {
                @UniqueConstraint(columnNames =
                        {
                                "username"
                        })
        })
@XmlRootElement
@NamedQueries(
        {
                @NamedQuery(name = "Authority.findAll", query = "SELECT u FROM Authority u"),
                @NamedQuery(name = "Authority.findByAuthorityId", query = "SELECT u FROM Authority u WHERE u.id = :id"),
                @NamedQuery(name = "Authority.findByAuthority", query = "SELECT u FROM Authority u WHERE u.authority = :authority")
        })
public class Authority implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id=null;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(nullable = false, length = 50)
    private String authority="USER";

    @JsonIgnore
    @JoinColumn(name = "username", referencedColumnName = "username", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    public Authority()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getAuthority()
    {
        return authority;
    }

    public void setAuthority(final String authority)
    {
        this.authority = authority;
    }

    @XmlTransient
    public User getUser()
    {
        return user;
    }

    public void setUser(final User user)
    {
        this.user = user;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Authority))
        {
            return false;
        }
        Authority other = (Authority) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.Authorities[ id=" + id + " ]";
    }

}
