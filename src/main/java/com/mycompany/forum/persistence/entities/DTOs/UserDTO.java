package com.mycompany.forum.persistence.entities.DTOs;

import com.mycompany.forum.persistence.entities.Message;
import com.mycompany.forum.persistence.entities.User;

import java.io.Serializable;
import java.util.List;

public class UserDTO implements Serializable
{
    private Long id;

    private String username;

    private String password;

    private List<Message> messageList;

    private List<TopicDTO> topicList;

    public UserDTO()
    {
    }

    public UserDTO(User user)
    {
        this.id = user.getId();
        this.username = user.getUsername();
//        this.password = user.getPassword();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public List<Message> getMessageList()
    {
        return messageList;
    }

    public void setMessageList(final List<Message> messageList)
    {
        this.messageList = messageList.size() == 0 ? null : messageList;
    }

    public List<TopicDTO> getTopicList()
    {
        return topicList;
    }

    public void setTopicList(final List<TopicDTO> topicList)
    {
        this.topicList = topicList.size() == 0 ? null : topicList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDTO))
        {
            return false;
        }
        UserDTO other = (UserDTO) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.User[ userId=" + id + " ]";
    }

}
