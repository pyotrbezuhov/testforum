package com.mycompany.forum.persistence.entities;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author User
 */
@Entity
@Table(name = "users", catalog = "", schema = "forum", uniqueConstraints =
        {
                @UniqueConstraint(columnNames =
                        {
                                "username"
                        })
        })
@XmlRootElement
@NamedQueries(
        {
                @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
                @NamedQuery(name = "User.findByUserId", query = "SELECT u FROM User u WHERE u.id = :id"),
                @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
                @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
                @NamedQuery(name = "User.findByEnabled", query = "SELECT u FROM User u WHERE u.enabled = :enabled")
        })
public class User implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Pattern(regexp = "([a-zA-Z]+\\s?)+", message = "Not a well-formed name")//,message = ""
    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Not a well-formed password")//,message = ""
    @Column(nullable = false, length = 50)
    private String password;

    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private Boolean enabled;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<Topic> topicList;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<Message> messageList;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<Authority> authorityList;

    public User()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    @XmlTransient
    public String getPassword()
    {
        return password;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }

    @XmlTransient
    public List<Message> getMessageList()
    {
        return messageList;
    }

    public void setMessageList(List<Message> messageList)
    {
        this.messageList = messageList;
    }

    @XmlTransient
    public List<Topic> getTopicList()
    {
        return topicList;
    }

    public void setTopicList(final List<Topic> topicList)
    {
        this.topicList = topicList;
    }

    @XmlTransient
    public List<Authority> getAuthorityList()
    {
        return authorityList;
    }

    public void setAuthorityList(final List<Authority> authorityList)
    {
        this.authorityList = authorityList;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User))
        {
            return false;
        }
        User other = (User) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString()
    {
        return "com.mycompany.forum.model.User[ id=" + id + " ]";
    }

}
