package com.mycompany.forum.persistence.entities.DTOs;

public class CustomDTO
{

	private String status;

	private String message;

	public CustomDTO()
	{
	}

	public CustomDTO(String status, String message)
	{
		this.status = status;
		this.message = message;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
