package com.mycompany.forum.persistence.services;

import com.mycompany.forum.persistence.ICRUDOperations;
import com.mycompany.forum.persistence.entities.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


public interface IMessageService extends ICRUDOperations<Message, Long>
{
    public Page<Message> findAllByPage(PageRequest pageRequest);
}
