package com.mycompany.forum.persistence.services;

import com.mycompany.forum.persistence.ICRUDOperations;
import com.mycompany.forum.persistence.entities.Authority;


public interface IAuthorityService extends ICRUDOperations<Authority, Long>
{
}
