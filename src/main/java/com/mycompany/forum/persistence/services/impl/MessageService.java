package com.mycompany.forum.persistence.services.impl;


import com.mycompany.forum.persistence.entities.Message;
import com.mycompany.forum.persistence.repositories.MessageRepository;
import com.mycompany.forum.persistence.services.common.AbstractService;
import com.mycompany.forum.persistence.services.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageService extends AbstractService<Message, Long> implements IMessageService
{
    @Autowired
    private MessageRepository messageRepository;

    @Override
    protected PagingAndSortingRepository<Message, Long> getRepository()
    {
        return messageRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Message> findAllByPage(PageRequest pageRequest)
    {
        return messageRepository.findAll(pageRequest);
    }
}
