package com.mycompany.forum.persistence.services.impl;


import com.mycompany.forum.persistence.entities.Topic;
import com.mycompany.forum.persistence.repositories.TopicRepository;
import com.mycompany.forum.persistence.services.common.AbstractService;
import com.mycompany.forum.persistence.services.ITopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TopicService extends AbstractService<Topic, Long> implements ITopicService
{
    @Autowired
    private TopicRepository topicRepository;

    @Override
    protected PagingAndSortingRepository<Topic, Long> getRepository()
    {
        return topicRepository;
    }

    @Override
    public Topic findTopicWithMessages(final Long id)
    {
        List<Topic> topicList = topicRepository.findTopicWithMessages(id);
        return topicList.size() == 0 ? null : topicList.get(0);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Topic> findAllByPage(PageRequest pageRequest)
    {
        return topicRepository.findAll(pageRequest);
    }
}
