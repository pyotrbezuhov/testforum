package com.mycompany.forum.persistence.services.common;

import com.google.common.collect.Lists;
import com.mycompany.forum.persistence.ICRUDOperations;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional
public abstract class AbstractService<T extends Serializable, ID extends Serializable> implements ICRUDOperations<T, ID>
{
	protected abstract PagingAndSortingRepository<T, ID> getRepository();

	@Override
	public T create(T entity)
	{
		return getRepository().save(entity);
	}

	@Transactional(readOnly = true)
	@Override
	public T findOne(ID entityId)
	{
		return getRepository().findOne(entityId);
	}

	@Transactional(readOnly = true)
	@Override
	public List<T> findAll()
	{
		return Lists.newArrayList(getRepository().findAll());
	}

	@Override
	public T update(T entity)
	{
		return getRepository().save(entity);
	}

	@Override
	public void delete(T entity)
	{
		getRepository().delete(entity);
	}

	@Override
	public void deleteById(ID entityId)
	{
		getRepository().delete(entityId);
	}
}
