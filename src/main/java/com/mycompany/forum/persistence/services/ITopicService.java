package com.mycompany.forum.persistence.services;

import com.mycompany.forum.persistence.ICRUDOperations;
import com.mycompany.forum.persistence.entities.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


public interface ITopicService extends ICRUDOperations<Topic, Long>
{
    public Topic findTopicWithMessages(Long id);

    public Page<Topic> findAllByPage(PageRequest pageRequest);
}
