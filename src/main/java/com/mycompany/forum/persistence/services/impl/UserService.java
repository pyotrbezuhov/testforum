package com.mycompany.forum.persistence.services.impl;


import com.mycompany.forum.persistence.entities.User;
import com.mycompany.forum.persistence.repositories.UserRepository;
import com.mycompany.forum.persistence.services.common.AbstractService;
import com.mycompany.forum.persistence.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User, Long> implements IUserService
{
    @Autowired
    private UserRepository userRepository;

    @Override
    protected PagingAndSortingRepository<User, Long> getRepository()
    {
        return userRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public User findByUserName(String username)
    {
        List<User> users = userRepository.findByUserName(username);

        return users.size() == 0 ? null : users.get(0);
    }

    @Transactional(readOnly = true)
    @Override
    public User findByUserNameAndPassword(String username, String password)
    {
        List<User> users = userRepository.findByUserNameAndPassword(username, password);

        return users.size() == 0 ? null : users.get(0);
    }


    @Transactional(readOnly = true)
    @Override
    public Page<User> findAllByPage(PageRequest pageRequest)
    {
        return userRepository.findAll(pageRequest);
    }
}
