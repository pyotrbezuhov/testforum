package com.mycompany.forum.persistence.services.impl;


import com.mycompany.forum.persistence.entities.Authority;
import com.mycompany.forum.persistence.repositories.AuthorityRepository;
import com.mycompany.forum.persistence.services.IAuthorityService;
import com.mycompany.forum.persistence.services.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthorityService extends AbstractService<Authority, Long> implements IAuthorityService
{
    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    protected PagingAndSortingRepository<Authority, Long> getRepository()
    {
        return authorityRepository;
    }
}
