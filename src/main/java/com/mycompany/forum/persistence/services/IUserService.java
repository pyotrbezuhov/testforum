package com.mycompany.forum.persistence.services;

import com.mycompany.forum.persistence.ICRUDOperations;
import com.mycompany.forum.persistence.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


public interface IUserService extends ICRUDOperations<User, Long>
{
    public User findByUserName(String username);

    public User findByUserNameAndPassword(String username, String password);

    public Page<User> findAllByPage(PageRequest pageRequest);

}
