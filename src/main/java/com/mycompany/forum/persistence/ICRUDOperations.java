package com.mycompany.forum.persistence;

import java.io.Serializable;
import java.util.List;

public interface ICRUDOperations<T extends Serializable, ID extends Serializable>
{
	T findOne(ID entityId);

	List<T> findAll();

	T create(final T entity);

	T update(final T entity);

	void delete(final T entity);

	void deleteById(final ID entityId);
}
