package com.mycompany.forum.web.util;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.Iterator;


public class MyConstraintViolation<T> implements ConstraintViolation<T>,Comparable<MyConstraintViolation>
{
	private String message;

	private String propertyPath;

	public MyConstraintViolation(String propertyPath, String message)
	{
		this.propertyPath = propertyPath;
		this.message = message;
	}


	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public String getMessageTemplate()
	{
		return null;
	}

	@Override
	public T getRootBean()
	{
		return null;
	}

	@Override
	public Class<T> getRootBeanClass()
	{
		return null;
	}

	@Override
	public Object getLeafBean()
	{
		return null;
	}

    @Override
    public Object[] getExecutableParameters()
    {
        return new Object[0];
    }

    @Override
    public Object getExecutableReturnValue()
    {
        return null;
    }

    @Override
	public Path getPropertyPath()
	{
		return new Path()
		{
			@Override
			public Iterator<Node> iterator()
			{
				return null;
			}

			public String toString()
			{
				return propertyPath;
			}
		};
	}

	@Override
	public Object getInvalidValue()
	{
		return null;
	}

	@Override
	public ConstraintDescriptor<?> getConstraintDescriptor()
	{
		return null;
	}

    @Override
    public <U> U unwrap(final Class<U> uClass)
    {
        return null;
    }

    @Override
	public int compareTo(MyConstraintViolation o)
	{
		return propertyPath.compareTo(o.propertyPath);
	}
}
