package com.mycompany.forum.web.util;

import com.mycompany.forum.persistence.entities.DTOs.CustomDTO;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler
{
    public RestResponseEntityExceptionHandler()
    {
        super();
    }

    // API

    // 400

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleBadRequest2(final RuntimeException ex, final WebRequest request) throws
            IOException
    {
        class ExceptionEntity
        {
            public String field;

            public String message;
        }

        org.hibernate.exception.ConstraintViolationException exception = (org.hibernate.exception.ConstraintViolationException) ex.getCause();

        ArrayList<ExceptionEntity> exceptionEntities = new ArrayList<ExceptionEntity>();

        ExceptionEntity exceptionEntity = new ExceptionEntity();

        exceptionEntity.field = exception.getConstraintName();
        exceptionEntity.message = exception.getCause().getMessage();
        exceptionEntities.add(exceptionEntity);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, exceptionEntities, headers, HttpStatus.BAD_REQUEST, request);
    }

    // API

    // 400

    @ExceptionHandler({ConstraintViolationException.class, TransactionSystemException.class})
    public ResponseEntity<Object> handleBadRequest(final RuntimeException ex, final WebRequest request)
    {
        class ExceptionEntity
        {
            public String field;

            public String message;
        }

        final ConstraintViolationException exception;
        if (ex instanceof TransactionSystemException)
        {
            exception = (ConstraintViolationException) ((TransactionSystemException) ex).getCause().getCause();
        } else
        {
            exception = (ConstraintViolationException) ex;
        }

        List<ConstraintViolation<?>> violations = new ArrayList<ConstraintViolation<?>>(exception.getConstraintViolations());

        ArrayList<ExceptionEntity> exceptionEntities = new ArrayList<ExceptionEntity>();

        for (ConstraintViolation<?> violation : violations)
        {
            ExceptionEntity exceptionEntity = new ExceptionEntity();

            exceptionEntity.field = violation.getPropertyPath().toString();
            exceptionEntity.message = violation.getMessage();

            exceptionEntities.add(exceptionEntity);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(ex, exceptionEntities, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request)
    {
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        //ex.getCause() instanceof JsonMappingException, JsonParseException // for additional information later on
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request)
    {
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request)
    {
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    // 403

    // 404

    @ExceptionHandler(value = {EntityNotFoundException.class, FileNotFoundException.class})
    protected ResponseEntity<Object> handleBadRequest403_404(final RuntimeException ex, final WebRequest request)
    {
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    // 409

    @ExceptionHandler({InvalidDataAccessApiUsageException.class, DataAccessException.class})
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request)
    {
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // 412

    // 500
    @ExceptionHandler({NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class})
    /*500*/ public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request)
    {
        logger.error("500 Status Code", ex);
        final CustomDTO bodyOfResponse = new CustomDTO("error", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
