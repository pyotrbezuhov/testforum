package com.mycompany.forum.web.util.grids;


import com.mycompany.forum.persistence.entities.DTOs.TopicDTO;

import java.util.List;

/**
 * Created by User on 3/22/14.
 */
public class TopicGrid
{
    private int totalPages;

    private int currentPage;

    private long totalRecords;

    private List<TopicDTO> topicData;

    public int getTotalPages()
    {
        return totalPages;
    }

    public void setTotalPages(final int totalPages)
    {
        this.totalPages = totalPages;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(final int currentPage)
    {
        this.currentPage = currentPage;
    }

    public long getTotalRecords()
    {
        return totalRecords;
    }

    public void setTotalRecords(final long totalRecords)
    {
        this.totalRecords = totalRecords;
    }

    public List<TopicDTO> getTopicData()
    {
        return topicData;
    }

    public void setTopicData(final List<TopicDTO> topicData)
    {
        this.topicData = topicData;
    }
}