package com.mycompany.forum.web.util;


public class MyDataAccessException extends Exception
{
	public MyDataAccessException()
	{
	}

	public MyDataAccessException(String message)
	{
		super(message);
	}

	public MyDataAccessException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public MyDataAccessException(Throwable cause)
	{
		super(cause);
	}

	public MyDataAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
