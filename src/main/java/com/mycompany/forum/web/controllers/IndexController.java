package com.mycompany.forum.web.controllers;

import com.mycompany.forum.persistence.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "")
public class IndexController
{
    @Autowired
    private IUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getHome(HttpSession session)
    {
        ModelAndView modelAndView = new ModelAndView("adminHome");
        return modelAndView;
    }
}