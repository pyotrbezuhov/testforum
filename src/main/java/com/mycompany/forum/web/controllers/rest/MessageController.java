package com.mycompany.forum.web.controllers.rest;


import com.mycompany.forum.persistence.entities.DTOs.MessageDTO;
import com.mycompany.forum.persistence.entities.Message;
import com.mycompany.forum.persistence.entities.Topic;
import com.mycompany.forum.persistence.entities.User;
import com.mycompany.forum.persistence.services.IMessageService;
import com.mycompany.forum.persistence.services.ITopicService;
import com.mycompany.forum.persistence.services.IUserService;
import com.mycompany.forum.web.util.grids.MessageGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
public class MessageController
{
    @Autowired
    private IUserService userService;

    @Autowired
    private ITopicService topicService;

    @Autowired
    private IMessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<List<MessageDTO>> findAll()
    {
        List<Message> messages = messageService.findAll();

        List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>(messages.size());
        for (Message message : messages) messageDTOList.add(new MessageDTO(message));

        return new ResponseEntity<List<MessageDTO>>(messageDTOList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<MessageDTO> findById(@PathVariable Long id)
    {
        Message message = messageService.findOne(id);
        if (message == null) throw new EntityNotFoundException();

        return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MessageDTO> create(@RequestBody Message message, Principal principal)
    {
        if (principal == null) return new ResponseEntity<MessageDTO>(HttpStatus.UNAUTHORIZED);
        if (message.getTopic() == null || message.getTopic().getId() == null)
            return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);

        final Topic topic = topicService.findOne(message.getTopic().getId());
        if (topic == null)
            return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);

        String username = principal.getName();
        User user = userService.findByUserName(username);
        message.setUser(user);
        message.setId(null);
        message.setCreateDate(new Date());
        message.setUpdateDate(null);
        message = messageService.create(message);

        return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<MessageDTO> update(@PathVariable Long id, @RequestBody Message newMessage, Principal principal)
    {
        if (principal == null) return new ResponseEntity<MessageDTO>(HttpStatus.UNAUTHORIZED);

        Message message = messageService.findOne(id);
        if (message == null) throw new EntityNotFoundException();
        if (!message.getUser().getUsername().equals(principal.getName()))
            return new ResponseEntity<MessageDTO>(HttpStatus.UNAUTHORIZED);

        message.setUpdateDate(new Date());
        message.setContent(newMessage.getContent());
        message = messageService.update(message);

        final MessageDTO messageDTO = new MessageDTO(message);
        return new ResponseEntity<MessageDTO>(messageDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id, Principal principal)
    {
        if (principal == null) return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        final Message message = messageService.findOne(id);
        if (message == null) throw new EntityNotFoundException();
        if (!message.getUser().getUsername().equals(principal.getName()))
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        messageService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = "/listGrid", method = RequestMethod.GET)
    public
    @ResponseBody
    MessageGrid listGrid(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                         @RequestParam(value = "rows", required = false) Integer rows,
                         @RequestParam(value = "sidx", required = false) String sortBy,
                         @RequestParam(value = "sord", required = false) String order)
    {
        // Process order by
        Sort sort = null;
        String orderBy = sortBy;
        //if (orderBy != null && orderBy.equals("birthDateString")) orderBy = "birthDate";

        if (orderBy != null && order != null)
        {
            if (order.equals("desc"))
            {
                sort = new Sort(Sort.Direction.DESC, orderBy);
            } else
                sort = new Sort(Sort.Direction.ASC, orderBy);
        }

        // Constructs page request for current page
        // Note: page number for Spring Data JPA starts with 0, while jqGrid starts with 1
        PageRequest pageRequest = sort == null ? new PageRequest(page - 1, rows) : new PageRequest(page - 1, rows, sort);

        Page<Message> messagePage = messageService.findAllByPage(pageRequest);

        // Construct the grid data that will return as JSON data
        MessageGrid messageGrid = new MessageGrid();

        messageGrid.setCurrentPage(messagePage.getNumber() + 1);
        messageGrid.setTotalPages(messagePage.getTotalPages());
        messageGrid.setTotalRecords(messagePage.getTotalElements());

        List<MessageDTO> messageList = new ArrayList<MessageDTO>();
        for (Message message : messagePage) messageList.add(new MessageDTO(message));

        messageGrid.setMessageData(messageList);

        return messageGrid;
    }


}
