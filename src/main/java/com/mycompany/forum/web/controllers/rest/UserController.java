package com.mycompany.forum.web.controllers.rest;


import com.mycompany.forum.persistence.entities.Authority;
import com.mycompany.forum.persistence.entities.DTOs.UserDTO;
import com.mycompany.forum.persistence.entities.User;
import com.mycompany.forum.persistence.services.IAuthorityService;
import com.mycompany.forum.persistence.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Controller
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController
{
    @Autowired
    private IUserService userService;

    @Autowired
    private IAuthorityService authorityService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<User>> findAll()
    {
        List<User> users = userService.findAll();
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<UserDTO> findById(@PathVariable Long id)
    {
        User user = userService.findOne(id);
        if (user == null) throw new EntityNotFoundException();

        return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserDTO> create(@RequestBody User user)
    {
        user.setId(null);
        user.setEnabled(true);
        user.setTopicList(null);
        user.setMessageList(null);
        user = userService.create(user);

        Authority authority = new Authority();
        authority.setUser(user);
        authority = authorityService.create(authority);

        return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserDTO> update(@PathVariable Long id, @RequestBody User newUser)
    {
        User user = userService.findOne(id);
        if (user == null) throw new EntityNotFoundException();
        user.setUsername(newUser.getUsername());
        user.setPassword(newUser.getPassword());
        user.setEnabled(newUser.getEnabled());
        user = userService.update(user);

        return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id)
    {
        userService.deleteById(id);

        return new ResponseEntity(HttpStatus.OK);
    }

}
