package com.mycompany.forum.web.controllers.rest;


import com.mycompany.forum.persistence.entities.DTOs.MessageDTO;
import com.mycompany.forum.persistence.entities.DTOs.TopicDTO;
import com.mycompany.forum.persistence.entities.DTOs.UserDTO;
import com.mycompany.forum.persistence.entities.Message;
import com.mycompany.forum.persistence.entities.Topic;
import com.mycompany.forum.persistence.entities.User;
import com.mycompany.forum.persistence.services.ITopicService;
import com.mycompany.forum.persistence.services.IUserService;
import com.mycompany.forum.web.util.grids.TopicGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/topics", produces = MediaType.APPLICATION_JSON_VALUE)
public class TopicController
{
    @Autowired
    private ITopicService topicService;

    @Autowired
    private IUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<TopicDTO>> findAll()
    {
        List<Topic> topics = topicService.findAll();

        List<TopicDTO> topicDTOList = new ArrayList<TopicDTO>(topics.size());
        for (Topic topic : topics)
        {
            final TopicDTO topicDTO = new TopicDTO(topic);
            topicDTO.setUser(new UserDTO(topic.getUser()));
            topicDTOList.add(topicDTO);
        }

        return new ResponseEntity<List<TopicDTO>>(topicDTOList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TopicDTO> findById(@PathVariable Long id)
    {
        Topic topic = topicService.findOne(id);
        if (topic == null) throw new EntityNotFoundException();

        final TopicDTO topicDTO = new TopicDTO(topic);
        return new ResponseEntity<TopicDTO>(topicDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/withMessages", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TopicDTO> findByIdWithMessages(@PathVariable Long id)
    {
        Topic topic = topicService.findTopicWithMessages(id);
        if (topic == null) throw new EntityNotFoundException();

        final TopicDTO topicDTO = new TopicDTO(topic);

        List<MessageDTO> messageDTOList = new ArrayList<MessageDTO>();
        for (Message message : topic.getMessageList())
            messageDTOList.add(new MessageDTO(message));

        topicDTO.setMessageList(messageDTOList);
        return new ResponseEntity<TopicDTO>(topicDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TopicDTO> create(@RequestBody Topic topic, HttpServletRequest request)
    {
        if (request.getUserPrincipal() == null) return new ResponseEntity<TopicDTO>(HttpStatus.UNAUTHORIZED);

        String username = request.getUserPrincipal().getName();
        User user = userService.findByUserName(username);
        topic.setUser(user);
        topic.setId(null);
        topic.setCreateDate(new Date());
        topic.setUpdateDate(null);
        topic.setMessageList(null);
        topic = topicService.create(topic);

        return new ResponseEntity<TopicDTO>(new TopicDTO(topic), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TopicDTO> update(@PathVariable Long id, @RequestBody(required = true) Topic topic, Principal principal)
    {
        if (principal == null) return new ResponseEntity<TopicDTO>(HttpStatus.UNAUTHORIZED);

        Topic newTopic = topicService.findOne(id);
        if (newTopic == null) throw new EntityNotFoundException();
        if (!newTopic.getUser().getUsername().equals(principal.getName()))
            return new ResponseEntity<TopicDTO>(HttpStatus.UNAUTHORIZED);

        newTopic.setContent(topic.getContent());
        newTopic.setUpdateDate(new Date());
        newTopic = topicService.update(newTopic);

        return new ResponseEntity<TopicDTO>(new TopicDTO(newTopic), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(@PathVariable("id") Long id, Principal principal)
    {
        if (principal == null) return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        final Topic newTopic = topicService.findOne(id);
        if (newTopic == null) throw new EntityNotFoundException();
        if (!newTopic.getUser().getUsername().equals(principal.getName()))
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);

        topicService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = "/listGrid", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<TopicGrid> listGrid(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                       @RequestParam(value = "rows", required = false) Integer rows,
                                       @RequestParam(value = "sidx", required = false) String sortBy,
                                       @RequestParam(value = "sord", required = false) String order)
    {
        // Process order by
        Sort sort = null;
        String orderBy = sortBy;
        //if (orderBy != null && orderBy.equals("birthDateString")) orderBy = "birthDate";

        if (orderBy != null && order != null)
        {
            if (order.equals("desc"))
            {
                sort = new Sort(Sort.Direction.DESC, orderBy);
            } else
                sort = new Sort(Sort.Direction.ASC, orderBy);
        }

        // Constructs page request for current page
        // Note: page number for Spring Data JPA starts with 0, while jqGrid starts with 1
        PageRequest pageRequest = sort == null ? new PageRequest(page - 1, rows) : new PageRequest(page - 1, rows, sort);

        Page<Topic> topicPage = topicService.findAllByPage(pageRequest);

        // Construct the grid data that will return as JSON data
        TopicGrid topicGrid = new TopicGrid();

        topicGrid.setCurrentPage(topicPage.getNumber() + 1);
        topicGrid.setTotalPages(topicPage.getTotalPages());
        topicGrid.setTotalRecords(topicPage.getTotalElements());

        List<TopicDTO> topicList = new ArrayList<TopicDTO>();
        for (Topic topic : topicPage) topicList.add(new TopicDTO(topic));

        topicGrid.setTopicData(topicList);

        return new ResponseEntity<TopicGrid>(topicGrid, HttpStatus.OK);
    }

}
