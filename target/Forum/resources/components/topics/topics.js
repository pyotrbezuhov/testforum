'use strict';
slApp.controller('topics', ['$scope', '$filter', '$state', '$log', '$http', function ($scope, $filter, $state, $log, $http) {
    $scope.loading = false;
    $scope.gridData = [];
    $scope.header = 'Topics';

    $scope.openEdit = function (id) {
        $state.transitionTo('topic', {id: id});
    }

    var buttonSelected = false;

    $scope.updateTopic = function (id) {
        $log.log("updateTopic ");
        buttonSelected = true;
        $http({method: 'PUT', url: 'topics/' + id}).success(function () {
            $log.log("---------------------------$http({method: 'PUT', url: 'topics/' + 1}).success------------------------------");
        }).error(function () {
                $log.log("---------------------------$http({method: 'PUT', url: 'topics/' + 1}).error------------------------------");
                document.location.href = "/login";
            });
    }
    $scope.deleteTopic = function (id, options) {
        $log.log("deleteTopic");
        buttonSelected = true;
        $http({method: 'DELETE', url: 'topics/' + id}).success(function () {
            $log.log("---------------------------$http({method: 'DELETE', url: 'topics/' + 1}).success------------------------------");
            options.colModel.hidden = true;
        }).error(function () {
                $log.log("---------------------------$http({method: 'DELETE', url: 'topics/' + 1}).error------------------------------");
                document.location.href = "/login";
            });
    }

    function updateFormatter(cellvalue, options, rowObject) {
        $log.log("updateFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);
        var result = '<button ng-click="updateTopic(' + cellvalue + ',' + rowObject + ')" class="fm-button" style="width: 100%; display: block;">Update</button>';
        return result;
    }

    function deleteFormatter(cellvalue, options, rowObject) {
        $log.log("deleteFormatter cellvalue %o,options %o,rowObject %o", cellvalue, options, rowObject);
        var result = '<button ng-click="deleteTopic(' + cellvalue + ',' + options + ')" class="fm-button" style="width: 100%; display: block;">Delete</button>';
        return result;
    }


    $("#topics-list").jqGrid({
        url: '/topics/listGrid',
        datatype: 'json',
        mtype: 'GET',
        colNames: ['Content', 'Create Date', 'Update Date', 'Update', 'Delete'],
        colModel: [
            {name: 'content', index: 'content', width: 800},
            {name: 'createDate', index: 'createDate', width: 100},
            {name: 'updateDate', index: 'updateDate', width: 100},
            {name: 'topicId', index: 'topicId', align: "left", width: 100, formatter: updateFormatter, search: false},
            {name: 'topicId', index: 'topicId', align: "left", width: 100, formatter: deleteFormatter, search: false},
        ],
        jsonReader: {
            root: "topicData",
            page: "currentPage",
            total: "totalPages",
            records: "totalRecords",
            viewrecords: true,
            repeatitems: false,
            id: "topicId"
        },
        pager: '#topics-pager',
        rowNum: 3,
        rowList: [3, 15, 20],
        sortname: 'createDate',
        sortorder: 'desc',
        viewrecords: true,
        gridview: true,
        height: 500,
        width: 1200,
        caption: 'Topics',
        onSelectRow: function (id) {
            $log.log("onselectRow %o", id);
            if (!buttonSelected) {
                $scope.openEdit(id);
                buttonSelected = false;
            }
        }
    });


}]);