'use strict';
slApp.controller('topic', ['$scope', '$filter', '$state', '$log', '$http', function ($scope, $filter, $state, $log, $http) {
    $scope.loading = false;
    $scope.data = {};
    $scope.url = 'topics/xx';
    $scope.header = 'Loading...';

    $scope.rows = [];
    $scope.configureRows = function () {
        if ($scope.data.messageList) {
            for (var i = 0, n = $scope.data.messageList.length; i < n; ++i) {
                $scope.data.messageList[i].createDate = new Date($scope.data.messageList[i].createDate);
                if ($scope.data.messageList[i].upateDate) {
                    $scope.data.messageList[i].upateDate = new Date($scope.data.messageList[i].upateDate);
                }
//                var row = [
//                    {
//                        dataContext: $scope.data.messageList[i],
//                        field: 'content',
//                        caption: 'Content',
//                        widgetType: 'text'
//                    },
//                    {
//                        dataContext: $scope.data.messageList[i],
//                        field: 'createDate',
//                        caption: 'Create Date',
//                        widgetType: 'createDate'
//                    },
//                    {
//                        dataContext: $scope.data.messageList[i],
//                        field: 'updateDate',
//                        caption: 'Update Date',
//                        widgetType: 'date'
//                    }
//                ];
            }
            $scope.rows = $scope.data.messageList;
        }

        $log.log('rows %o', $scope.rows);
    };


    $scope._state = $state;
    $scope.$watch('_state.params', function (newvalue, oldvalue) {
        $scope.loading = true;
        if (newvalue.id) {
            $scope.header = 'Loading';
            $scope.url = 'topics/' + newvalue.id;
            $http.get($scope.url).then(function (result) {
                $log.log(result);
                $scope.loading = false;
                $scope.header = result.content;
                $scope.data = result.data;
                $scope.configureRows();
            });
        } else {
            $scope.header = 'Invalid Topic';
            $scope.loading = false;
        }
    });
}]);