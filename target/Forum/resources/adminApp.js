'use strict';

var slApp = angular.module('slApp', ['ngGrid', 'ngQuickDate', 'ui.utils', 'ui.bootstrap', 'ui.router', 'ui.select2']);

// bootstrap (pre instance)
slApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider
        .otherwise('/topics');

    $httpProvider.defaults.headers.patch = {
        'Content-Type': 'application/json'
    };

    //////////////////////////Topic////////////////////////////////
    $stateProvider.state('topics', {
        url: '/topics',
        templateUrl: 'resources/components/topics/topics.html',
        controller: 'topics'
    });
    $stateProvider.state('topic', {
        url: '/topics/{id}',
        templateUrl: 'resources/components/topic/topic.html',
        controller: 'topic'
    });
}]);


// once app is instantiated
slApp.run(['constants', '$http', function (constants) {

    // set window title
    document.title = constants.appTitle;
}]);