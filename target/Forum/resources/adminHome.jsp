<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Forum</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- css -->
    <link href="resources/bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="resources/bower_components/angular-grid/ng-grid.min.css" type="text/css" rel="stylesheet" />

    <!-- jQuery  -->
    <script src="resources/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- angular framework-->
    <script src="resources/bower_components/angular/angular.min.js" type="text/javascript"></script>
    <script src="resources/bower_components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
    <script src="resources/bower_components/angular-ui-utils/ui-utils.min.js" type="text/javascript"></script>
    <script src="resources/bower_components/angular-grid/ng-grid-2.0.7.min.js" type="text/javascript"></script>
    <script src="resources/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
    <script src="resources/bower_components/angular-deckgrid/angular-deckgrid.js" type="text/javascript"></script>

    <!-- ngQuickDate -->
    <script src="resources/bower_components/ngQuickDate/dist/ng-quick-date.js" type="text/javascript"></script>
    <link rel="stylesheet" href="resources/bower_components/ngQuickDate/dist/ng-quick-date-plus-default-theme.css">

    <!-- angular select 2-->
    <link rel="stylesheet" href="resources/bower_components/select2/select2.css">
    <script type="text/javascript" src="resources/bower_components/select2/select2.js"></script>
    <script type="text/javascript" src="resources/bower_components/angular-ui-select2/src/select2.js"></script>

    <%--app modules, components, directives, services, etc--%>
    <script src="resources/adminApp.js" type="text/javascript"></script>
    <script src="resources/constants.js" type="text/javascript"></script>

    <script src="resources/components/topics/topics.js" type="text/javascript"></script>
    <script src="resources/components/topic/topic.js" type="text/javascript"></script>
</head>
<body ng-app="slApp">
<div class="container-fluid">
    <div ui-view></div>
</div>
</body>

</html>